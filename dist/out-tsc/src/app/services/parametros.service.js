import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
var ParametrosService = /** @class */ (function () {
    function ParametrosService() {
        this.dataSource = new BehaviorSubject("default message");
        this.serviceData = this.dataSource.asObservable();
    }
    ParametrosService.prototype.changeData = function (data) {
        this.dataSource.next(data);
    };
    ParametrosService.prototype.dataProducto = function (product) {
        this.dataSource.next(product);
    };
    ParametrosService.prototype.dataPost = function (post) {
        this.dataSource.next(post);
    };
    ParametrosService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ParametrosService);
    return ParametrosService;
}());
export { ParametrosService };
//# sourceMappingURL=parametros.service.js.map