var FirebaseUserModel = /** @class */ (function () {
    function FirebaseUserModel() {
        this.image = "";
        this.name = "";
        this.provider = "";
        this.email = "";
        this.uid = "";
        this.telefono = "";
    }
    return FirebaseUserModel;
}());
export { FirebaseUserModel };
//# sourceMappingURL=user-model.js.map