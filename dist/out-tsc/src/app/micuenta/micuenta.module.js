import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MicuentaPage } from './micuenta.page';
var routes = [
    {
        path: '',
        component: MicuentaPage
    }
];
var MicuentaPageModule = /** @class */ (function () {
    function MicuentaPageModule() {
    }
    MicuentaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MicuentaPage]
        })
    ], MicuentaPageModule);
    return MicuentaPageModule;
}());
export { MicuentaPageModule };
//# sourceMappingURL=micuenta.module.js.map