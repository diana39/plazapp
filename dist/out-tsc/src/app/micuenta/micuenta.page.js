import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
var MicuentaPage = /** @class */ (function () {
    function MicuentaPage(auth, menuCtrl, server, nav, userService, navCtrl) {
        var _this = this;
        this.auth = auth;
        this.menuCtrl = menuCtrl;
        this.server = server;
        this.nav = nav;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.userService.getCurrentUser()
            .then(function (user) {
            _this.name = user.name;
            _this.email = user.email;
            _this.tel = user.telefono;
            _this.userimg = user.image;
            if (user.provider == "facebook.com") {
                _this.social = 'facebook';
                _this.pass = 'social';
            }
            else if (user.provider == "google.com") {
                _this.social = 'google';
                _this.pass = 'social';
            }
            else {
                _this.social = 'none';
                _this.pass = '1234';
            }
            console.log(user);
        });
    }
    MicuentaPage.prototype.ngOnInit = function () { };
    MicuentaPage.prototype.ionViewWillEnter = function () {
        localStorage.setItem("fromCart", "false");
    };
    MicuentaPage.prototype.actualizarUsuario = function () {
        var _this = this;
        console.log("Actualizando Usuario");
        this.userService.getCurrentUser()
            .then(function (user) {
            if (user.provider == "facebook.com" || user.provider == "google.com") {
                user.telefono = _this.tel;
            }
            else {
                user.telefono = _this.tel;
                user.name = _this.name;
            }
            console.log(user);
            _this.server.updateUser(user).then(function (data) {
                _this.server.showAlert("Usuario Actualizado", "Usuario actualizado con éxito");
            }).catch(function (err) {
                _this.server.showAlert("Error al Actualizar", "Ha ocurrido un error al actualizar la información del usuario ");
            });
        });
    };
    MicuentaPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    MicuentaPage.prototype.goTarjetas = function () {
        this.nav.navigateRoot("tarjetas");
    };
    MicuentaPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.navigateRoot("intro");
    };
    MicuentaPage.prototype.fbLogin = function () {
        var _this = this;
        console.log("Facebook Login Reconnect");
        this.auth.signOut();
        this.auth.doFacebookLogin().then(function (resp) {
            console.log("Autenticado:", resp);
            _this.navCtrl.navigateRoot("tabs/home");
        }).catch(function (err) {
            _this.server.showAlert("Error al Ingresar", "Error al ingresar con Facebook, intente nuevamente.");
            console.log("error:", err);
        });
    };
    MicuentaPage.prototype.googleLogin = function () {
        var _this = this;
        console.log("Google Login Reconnect");
        this.auth.signOut();
        this.auth.doGoogleLogin().then(function (resp) {
            console.log("Autenticado:", resp);
            _this.navCtrl.navigateRoot("tabs/home");
        }).catch(function (err) {
            _this.server.showAlert("Error al Ingresar", "Error al ingresar con Google, intente nuevamente.");
            console.log("error:", err);
        });
    };
    MicuentaPage = tslib_1.__decorate([
        Component({
            selector: 'app-micuenta',
            templateUrl: './micuenta.page.html',
            styleUrls: ['./micuenta.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AuthService,
            MenuController,
            ServerService,
            NavController,
            UserService,
            NavController])
    ], MicuentaPage);
    return MicuentaPage;
}());
export { MicuentaPage };
//# sourceMappingURL=micuenta.page.js.map