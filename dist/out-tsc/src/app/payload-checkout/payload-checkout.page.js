import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
var PayloadCheckoutPage = /** @class */ (function () {
    function PayloadCheckoutPage(navCtrl, menuCtrl, auth, server, user, alertCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.server = server;
        this.user = user;
        this.alertCtrl = alertCtrl;
        this.method = "Selecciona...";
        this.nombredia = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];
        this.nombremethod = ["Link de pago", "Tarjeta", "Datáfono"];
        this.bolsa = "0";
        this.otherpropina = "Otro";
        this.valor_bolsa = 0;
        this.valor_bolsa_setting = 0;
        this.payload = {
            "metodoPago": "",
            "horarioRangoIni": "dd/mm/aaaa hh:mm",
            "horarioRangoFin": "dd/mm/aaaa hh:mm",
            "tarjetaId": "",
            "propina": "",
            "bolsa": "",
            "cuotas": "",
            "rangoId": "0",
            "comentarios": "",
            "fechaSolicitud": "dd/mm/aaaa hh:mm",
            "idDireccion": "",
            "uid": "",
            "total": "",
            "subtotal": "",
            "cupon_id": "",
            "productos": []
        };
    }
    PayloadCheckoutPage.prototype.ngOnInit = function () {
    };
    PayloadCheckoutPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var dir = parseInt(localStorage.getItem("address"));
        //Get Range Time
        var rang = localStorage.getItem("range-time");
        var rangsplit = rang.split("|");
        var dia = new Date(rangsplit[0]);
        var ini = new Date(rangsplit[1]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        var fini = new Date(rangsplit[2]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        this.range = this.nombredia[dia.getDay()] + " " + ini + " a " + fini;
        //Get Method
        var methodactual = parseInt(localStorage.getItem("method"));
        if (!isNaN(methodactual)) {
            this.method = this.nombremethod[methodactual];
            if (methodactual == 1) {
                var tarjetanombre = localStorage.getItem("tarjeta_nombre").split("|");
                this.method = tarjetanombre[0];
                this.methodtipo = tarjetanombre[1];
            }
        }
        //Get Propina
        var propinaactual = parseInt(localStorage.getItem("propina"));
        if (!isNaN(propinaactual)) {
            this.propina = propinaactual;
            if (propinaactual > 1000) {
                this.otherpropina = "$" + String(propinaactual);
            }
        }
        else {
            this.propina = 1000;
        }
        //Get Valor Bolsa
        this.auth.getGenKeyVal("Settings", "llave", "valor_bolsa").then(function (data) {
            var indata;
            indata = data;
            _this.valor_bolsa_setting = indata[0].valor;
            var bolsaactual = parseInt(localStorage.getItem("bolsa"));
            if (!isNaN(bolsaactual)) {
                _this.bolsa = String(bolsaactual);
                if (_this.bolsa == "0") {
                    _this.valor_bolsa = 0;
                }
                else {
                    _this.valor_bolsa = _this.valor_bolsa_setting;
                }
            }
        });
        //Get Bolsa
        //Set Subtotal, Envio, Total;
        this.subtotal = localStorage.getItem("subtotal");
        this.envio = localStorage.getItem("envio");
        this.total = parseInt(this.subtotal) + parseInt(this.envio);
    };
    PayloadCheckoutPage.prototype.setRange = function () {
        this.navCtrl.navigateBack("payload-program");
    };
    PayloadCheckoutPage.prototype.setMethod = function () {
        this.navCtrl.navigateForward("tipo-pago");
    };
    PayloadCheckoutPage.prototype.setPropina = function (value) {
        this.propina = value;
        var propinacheck = parseInt(value);
        if (propinacheck > 1000) {
            this.presentAlertPropina();
        }
        else {
            this.otherpropina = "Otro";
            localStorage.setItem("propina", value);
        }
    };
    PayloadCheckoutPage.prototype.setBolsa = function (value) {
        this.bolsa = value;
        localStorage.setItem("bolsa", value);
        if (value == "0") {
            this.valor_bolsa = 0;
        }
        else {
            this.valor_bolsa = this.valor_bolsa_setting;
        }
    };
    PayloadCheckoutPage.prototype.checkout = function () {
        var _this = this;
        //this.user.getCurrentUser().then(function (user) {
            _this.payload.uid = localStorage.uid;
            _this.payload.bolsa = _this.bolsa;
            _this.payload.propina = _this.propina;
            _this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
            _this.payload.idDireccion = localStorage.getItem("address");
            _this.payload.tarjetaId = localStorage.getItem("tarjeta_actual");
            _this.payload.subtotal = localStorage.getItem("subtotal");
            _this.payload.total = _this.total;
            var cart = JSON.parse(localStorage.getItem('cart'));
            var numcart = JSON.parse(localStorage.getItem('numcart'));
            var unitcart = JSON.parse(localStorage.getItem('unitcart'));
            var preciocart = JSON.parse(localStorage.getItem('preciocart'));
            for (var i = 0; i < cart.length; i++) {
                var productocart = {};
                productocart["id"] = cart[i];
                productocart["numcart"] = numcart[i];
                productocart["unitcart"] = unitcart[i];
                productocart["precio"] = preciocart[i];
                _this.payload.productos.push(productocart);
            }
            console.log("Log Pedido", _this.payload);
            _this.server.addPedido(_this.payload).then(function (data) {
                if (data["ok"]) {
                    _this.navCtrl.navigateForward("payload-done");
                }
                else {
                    _this.server.showAlert("Error de Pedido", data["error_msg"]);
                }
            }).catch(function (err) {
                _this.server.showAlert("Error de Pedido", "Ha ocurrido un error al solicitar el pedido");
            });
        //});
    };
    PayloadCheckoutPage.prototype.checkTarjeta = function () {
        var methodactual = parseInt(localStorage.getItem("method"));
        if (!isNaN(methodactual)) {
            if (methodactual == 1) {
                return false;
            }
            else {
                return true;
            }
        }
    };
    PayloadCheckoutPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PayloadCheckoutPage.prototype.presentAlertPropina = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Ingresa tu propina',
                            inputs: [
                                {
                                    name: 'otherpropina',
                                    placeholder: '$0'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancelar',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                        _this.propina = "1000";
                                        localStorage.setItem("propina", "1000");
                                    }
                                },
                                {
                                    text: 'Guardar',
                                    handler: function (data) {
                                        _this.propina = data.otherpropina;
                                        _this.otherpropina = "$" + data.otherpropina;
                                        localStorage.setItem("propina", data.otherpropina);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PayloadCheckoutPage = tslib_1.__decorate([
        Component({
            selector: 'app-payload-checkout',
            templateUrl: './payload-checkout.page.html',
            styleUrls: ['./payload-checkout.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            AuthService,
            ServerService,
            UserService,
            AlertController])
    ], PayloadCheckoutPage);
    return PayloadCheckoutPage;
}());
export { PayloadCheckoutPage };
//# sourceMappingURL=payload-checkout.page.js.map