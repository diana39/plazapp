import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PayloadCheckoutPage } from './payload-checkout.page';
import { ComponentsModule } from '../componentes/componentes.module';
var routes = [
    {
        path: '',
        component: PayloadCheckoutPage
    }
];
var PayloadCheckoutPageModule = /** @class */ (function () {
    function PayloadCheckoutPageModule() {
    }
    PayloadCheckoutPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ComponentsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PayloadCheckoutPage]
        })
    ], PayloadCheckoutPageModule);
    return PayloadCheckoutPageModule;
}());
export { PayloadCheckoutPageModule };
//# sourceMappingURL=payload-checkout.module.js.map