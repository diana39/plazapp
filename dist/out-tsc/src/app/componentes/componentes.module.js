import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from "@angular/forms";
import { DireccionesComponent } from './direcciones/direcciones.component';
var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule.forRoot(),
            ],
            declarations: [
                DireccionesComponent
            ],
            exports: [
                DireccionesComponent
            ],
            entryComponents: [],
        })
    ], ComponentsModule);
    return ComponentsModule;
}());
export { ComponentsModule };
//# sourceMappingURL=componentes.module.js.map