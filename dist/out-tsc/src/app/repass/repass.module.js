import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RepassPage } from './repass.page';
var routes = [
    {
        path: '',
        component: RepassPage
    }
];
var RepassPageModule = /** @class */ (function () {
    function RepassPageModule() {
    }
    RepassPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RepassPage]
        })
    ], RepassPageModule);
    return RepassPageModule;
}());
export { RepassPageModule };
//# sourceMappingURL=repass.module.js.map