import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PayloadDonePage } from './payload-done.page';
var routes = [
    {
        path: '',
        component: PayloadDonePage
    }
];
var PayloadDonePageModule = /** @class */ (function () {
    function PayloadDonePageModule() {
    }
    PayloadDonePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PayloadDonePage]
        })
    ], PayloadDonePageModule);
    return PayloadDonePageModule;
}());
export { PayloadDonePageModule };
//# sourceMappingURL=payload-done.module.js.map