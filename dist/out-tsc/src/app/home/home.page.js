import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NavController, LoadingController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ParametrosService } from '../services/parametros.service';
var HomePage = /** @class */ (function () {
    function HomePage(screenOrientation, navCtrl, alertCtrl, auth, db, loadingCtrl, menuCtrl, param, platform) {
        this.screenOrientation = screenOrientation;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.db = db;
        this.loadingCtrl = loadingCtrl;
        this.menuCtrl = menuCtrl;
        this.param = param;
        this.platform = platform;
        this.menuCtrl.enable(true, 'mimenu');
        if (this.platform.is('cordova')) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        }
        this.getCategorias();
        this.getProductos();
        var cart = JSON.parse(localStorage.getItem('cart'));
        if (cart === undefined || cart === null || cart.length === 0) {
            cart = [];
            var numcart = [];
            var unitcart = [];
            var badgecart = "0";
            var preciocart = [];
            localStorage.setItem("cart", JSON.stringify(cart));
            localStorage.setItem("numcart", JSON.stringify(numcart));
            localStorage.setItem("unitcart", JSON.stringify(unitcart));
            localStorage.setItem("badgecart", badgecart);
            localStorage.setItem("preciocart", JSON.stringify(preciocart));
            console.log("Cart is created", cart);
        }
        else {
            console.log("Cart already created", cart);
        }
    }
    HomePage.prototype.ionViewWillEnter = function () {
        localStorage.setItem("fromCart", "false");
        localStorage.setItem("fromSearch", "false");
    };
    HomePage.prototype.bienvenido = function () {
        this.navCtrl.navigateForward('/bienvenido');
    };
    HomePage.prototype.intro = function () {
        this.navCtrl.navigateForward('/intro');
    };
    HomePage.prototype.goCarrito = function () {
        this.navCtrl.navigateForward('/cart');
    };
    HomePage.prototype.goShop = function (id) {
        this.param.changeData(id);
        console.log("Categoria:", id);
        this.navCtrl.navigateForward('tabs/shop');
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('Home Page');
    };
    HomePage.prototype.getCategorias = function () {
        var me = this;
        this.auth.getGenOV('Categoria').then(function (data) {
            me.categorias = data;
            localStorage.setItem('categorias', JSON.stringify(data));
            console.log("Categorias: ", me.categorias);
        });
    };
    HomePage.prototype.getProductos = function () {
        var me = this;
        this.auth.getGenOV('Producto').then(function (data) {
            me.productos = data;
            localStorage.setItem('productos', JSON.stringify(data));
            console.log("Productos: ", me.productos);
        });
    };
    HomePage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ScreenOrientation,
            NavController,
            AlertController,
            AuthService,
            AngularFireDatabase,
            LoadingController,
            MenuController,
            ParametrosService,
            Platform])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map