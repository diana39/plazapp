import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TerminosPage } from './terminos.page';
var routes = [
    {
        path: '',
        component: TerminosPage
    }
];
var TerminosPageModule = /** @class */ (function () {
    function TerminosPageModule() {
    }
    TerminosPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TerminosPage]
        })
    ], TerminosPageModule);
    return TerminosPageModule;
}());
export { TerminosPageModule };
//# sourceMappingURL=terminos.module.js.map