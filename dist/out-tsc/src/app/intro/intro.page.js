import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
var IntroPage = /** @class */ (function () {
    function IntroPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'mimenu');
    }
    IntroPage.prototype.ngOnInit = function () {
        console.log('IntroPage');
    };
    IntroPage.prototype.goReg = function () {
        this.navCtrl.navigateForward("/registro");
    };
    IntroPage.prototype.goLog = function () {
        this.navCtrl.navigateForward("/log");
    };
    IntroPage = tslib_1.__decorate([
        Component({
            selector: 'app-intro',
            templateUrl: './intro.page.html',
            styleUrls: ['./intro.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, MenuController])
    ], IntroPage);
    return IntroPage;
}());
export { IntroPage };
//# sourceMappingURL=intro.page.js.map