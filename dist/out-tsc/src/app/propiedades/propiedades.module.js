import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PropiedadesPage } from './propiedades.page';
var routes = [
    {
        path: '',
        component: PropiedadesPage
    }
];
var PropiedadesPageModule = /** @class */ (function () {
    function PropiedadesPageModule() {
    }
    PropiedadesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PropiedadesPage]
        })
    ], PropiedadesPageModule);
    return PropiedadesPageModule;
}());
export { PropiedadesPageModule };
//# sourceMappingURL=propiedades.module.js.map