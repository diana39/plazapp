import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
var PropiedadesPage = /** @class */ (function () {
    function PropiedadesPage(param, navCtrl) {
        this.param = param;
        this.navCtrl = navCtrl;
    }
    PropiedadesPage.prototype.ngOnInit = function () {
    };
    PropiedadesPage.prototype.ionViewWillEnter = function () {
        this.producto = JSON.parse(localStorage.getItem('producto-actual'))[0];
        console.log("producto", this.producto);
    };
    PropiedadesPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PropiedadesPage = tslib_1.__decorate([
        Component({
            selector: 'app-propiedades',
            templateUrl: './propiedades.page.html',
            styleUrls: ['./propiedades.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ParametrosService, NavController])
    ], PropiedadesPage);
    return PropiedadesPage;
}());
export { PropiedadesPage };
//# sourceMappingURL=propiedades.page.js.map