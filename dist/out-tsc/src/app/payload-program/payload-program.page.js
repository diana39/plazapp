import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
var PayloadProgramPage = /** @class */ (function () {
    function PayloadProgramPage(navCtrl, menuCtrl, server, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.server = server;
        this.auth = auth;
        this.error = true;
        this.error_msg = "";
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        //this.dia = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
        //this.ini = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
        var now = new Date(Date.now() - tzoffset).getTime();
        //let oneHoursIntoFuture = new Date(now + 1000 * 60 * 60 * 1);
        //this.fini = oneHoursIntoFuture.toISOString().slice(0, -1);
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        var firstDate = new Date(y, m, 1);
        var lastDate = new Date(y, m + 1, 0);
        this.min = firstDate.toISOString().slice(0, -1);
        this.max = lastDate.toISOString().slice(0, -1);
        //Get setting for days
        this.auth.getGenKeyVal("Settings", "llave", "dias_habiles").then(function (data) {
            var indata;
            indata = data;
            _this.diashabiles = indata[0].valor;
        });
        this.auth.getGenKeyVal("Settings", "llave", "tiempo_minimo").then(function (data) {
            var indata;
            indata = data;
            _this.hourinterval = indata[0].valor;
        });
        this.auth.getGenKeyVal("Settings", "llave", "horas_habiles").then(function (data) {
            var indata;
            indata = data;
            var mininidata = indata[0].valor.split(",");
            var databuilder = "";
            var mininidata2 = parseInt(mininidata[0]) + parseInt(_this.hourinterval) - 1;
            for (var i = mininidata2; i < mininidata[1]; i++) {
                databuilder += (i - 1) + ",";
                _this.hourvaluesini = databuilder;
            }
            databuilder = "";
            for (var i = mininidata2 + 1; i <= mininidata[1]; i++) {
                databuilder += i + ",";
                _this.hourvaluesfini = databuilder;
            }
            //this.ini = new Date(new Date(y,m,d,mininidata[0],0,0).getTime()-tzoffset).toISOString().slice(0, -1);
            //this.fini = new Date(new Date(y,m,d,mininidata[1],0,0).getTime()-tzoffset).toISOString().slice(0, -1);
        });
    }
    PayloadProgramPage.prototype.ngOnInit = function () {
    };
    PayloadProgramPage.prototype.checkDate = function () {
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        var now = (new Date(Date.now() - tzoffset).getTime());
        var dianow = (new Date(this.dia).getTime());
        var houri = new Date(this.dia + " " + this.ini).getHours();
        var hourf = new Date(this.dia + " " + this.fini).getHours();
        var hourint = hourf - houri;
        if ((dianow - now) < 0) {
            this.error = true;
            this.error_msg = "La fecha seleccionada es menor a la fecha actual.";
        }
        else if (isNaN(hourint)) {
            this.error = true;
            this.error_msg = "La horas seleccionadas es menor al intervalo minimo: " + this.hourinterval + " Horas.";
        }
        else if ((hourint) < this.hourinterval) {
            this.error = true;
            this.error_msg = "La hora no han sido ajustadas";
        }
        else {
            this.error = false;
        }
    };
    PayloadProgramPage.prototype.continueCart = function () {
        if (this.error) {
            this.server.showAlert("Error de Programación", this.error_msg);
        }
        else {
            var range = void 0;
            range = this.dia + "|" + this.ini + "|" + this.fini;
            localStorage.setItem("range-time", range);
            console.log("Range", range);
            this.navCtrl.navigateForward("payload-checkout");
        }
    };
    PayloadProgramPage.prototype.goBack = function () {
        this.navCtrl.navigateBack("tabs/cart");
    };
    PayloadProgramPage = tslib_1.__decorate([
        Component({
            selector: 'app-payload-program',
            templateUrl: './payload-program.page.html',
            styleUrls: ['./payload-program.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            ServerService,
            AuthService])
    ], PayloadProgramPage);
    return PayloadProgramPage;
}());
export { PayloadProgramPage };
//# sourceMappingURL=payload-program.page.js.map