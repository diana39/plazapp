import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { AuthService } from '../services/auth.service';
var CartPage = /** @class */ (function () {
    function CartPage(navCtrl, param, menuCtrl, auth) {
        this.navCtrl = navCtrl;
        this.param = param;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.tempproductos = [];
        this.finalproductos = [];
        this.envio = 10000;
        this.total = 0;
        this.subtotal = 0;
    }
    CartPage.prototype.ngOnInit = function () {
    };
    CartPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //Get List of Tallas
        this.auth.getGenOV("Talla").then(function (data) {
            _this.tallas = data;
        });
        //Get setting for envio
        this.auth.getGenKeyVal("Settings", "llave", "valor_domicilio").then(function (data) {
            var indata;
            indata = data;
            console.log("envio", indata[0].valor);
            _this.envio = parseInt(indata[0].valor);
        });
        //Get carts values
        this.cart = JSON.parse(localStorage.getItem('cart'));
        this.numcart = JSON.parse(localStorage.getItem('numcart'));
        this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
        this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
        this.productos = JSON.parse(localStorage.getItem('productos'));
        console.log("unit", this.unitcart);
        console.log("num", this.numcart);
        this.filterProductos();
        this.calculateTotal();
    };
    CartPage.prototype.calculateTotal = function () {
        this.subtotal = 0;
        for (var i = 0; i < this.preciocart.length; i++) {
            this.subtotal = this.subtotal + parseInt(this.preciocart[i]);
        }
        this.total = this.subtotal + this.envio;
        localStorage.setItem("subtotal", String(this.subtotal));
        localStorage.setItem("envio", String(this.envio));
    };
    CartPage.prototype.removeCart = function (index) {
        console.log("id remove:", index);
        this.cart = JSON.parse(localStorage.getItem('cart'));
        this.numcart = JSON.parse(localStorage.getItem('numcart'));
        this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
        this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
        this.cart.splice(index, 1);
        this.numcart.splice(index, 1);
        this.unitcart.splice(index, 1);
        this.preciocart.splice(index, 1);
        localStorage.setItem("cart", JSON.stringify(this.cart));
        localStorage.setItem("numcart", JSON.stringify(this.numcart));
        localStorage.setItem("unitcart", JSON.stringify(this.unitcart));
        localStorage.setItem("preciocart", JSON.stringify(this.preciocart));
        console.log("cart", this.cart);
        console.log("numcart", this.numcart);
        console.log("unitcart", this.unitcart);
        console.log("preciocart", this.preciocart);
        this.filterProductos();
        this.calculateTotal();
        this.reloadCart();
    };
    CartPage.prototype.filterProductos = function () {
        var _this = this;
        this.finalproductos = [];
        var _loop_1 = function (i) {
            this_1.finalproductos[i] = this_1.productos.filter(function (item) { return item.id === _this.cart[i]; })[0];
        };
        var this_1 = this;
        for (var i = 0; i < this.cart.length; i++) {
            _loop_1(i);
        }
        console.log("cart", this.cart);
        console.log("nofilter", this.productos);
        console.log("filter", this.finalproductos);
        localStorage.setItem("badgecart", this.cart.length);
    };
    CartPage.prototype.clearCart = function () {
        this.finalproductos = [];
        this.cart = [];
        this.numcart = [];
        this.unitcart = [];
        this.preciocart = [];
        localStorage.setItem("cart", JSON.stringify(this.cart));
        localStorage.setItem("numcart", JSON.stringify(this.numcart));
        localStorage.setItem("unitcart", JSON.stringify(this.unitcart));
        localStorage.setItem("preciocart", JSON.stringify(this.preciocart));
        this.filterProductos();
        this.calculateTotal();
        this.reloadCart();
    };
    CartPage.prototype.continueCart = function () {
        this.navCtrl.navigateForward("payload-program");
    };
    CartPage.prototype.reloadCart = function () {
        this.navCtrl.navigateRoot("tabs/cart");
    };
    CartPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    CartPage = tslib_1.__decorate([
        Component({
            selector: 'app-cart',
            templateUrl: './cart.page.html',
            styleUrls: ['./cart.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ParametrosService,
            MenuController,
            AuthService])
    ], CartPage);
    return CartPage;
}());
export { CartPage };
//# sourceMappingURL=cart.page.js.map