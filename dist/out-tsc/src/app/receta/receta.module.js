import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RecetaPage } from './receta.page';
var routes = [
    {
        path: '',
        component: RecetaPage
    }
];
var RecetaPageModule = /** @class */ (function () {
    function RecetaPageModule() {
    }
    RecetaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RecetaPage]
        })
    ], RecetaPageModule);
    return RecetaPageModule;
}());
export { RecetaPageModule };
//# sourceMappingURL=receta.module.js.map