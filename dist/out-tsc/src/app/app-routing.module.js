import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
var routes = [
    { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
    //{ path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './home/home.module#HomePageModule' },
    { path: 'ayuda', loadChildren: './ayuda/ayuda.module#AyudaPageModule' },
    { path: 'bienvenido', loadChildren: './bienvenido/bienvenido.module#BienvenidoPageModule' },
    { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
    { path: 'log', loadChildren: './log/log.module#LogPageModule' },
    { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
    { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
    { path: 'repass', loadChildren: './repass/repass.module#RepassPageModule' },
    { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
    { path: 'terminos', loadChildren: './terminos/terminos.module#TerminosPageModule' },
    { path: 'buscar', loadChildren: './buscar/buscar.module#BuscarPageModule' },
    { path: 'shop', loadChildren: './shop/shop.module#ShopPageModule' },
    { path: 'contacto', loadChildren: './contacto/contacto.module#CONTACTOPageModule' },
    { path: 'addproducto', loadChildren: './addproducto/addproducto.module#AddproductoPageModule' },
    { path: 'micuenta', loadChildren: './micuenta/micuenta.module#MicuentaPageModule' },
    { path: 'recetas', loadChildren: './recetas/recetas.module#RecetasPageModule' },
    { path: 'receta', loadChildren: './receta/receta.module#RecetaPageModule' },
    { path: 'propiedades', loadChildren: './propiedades/propiedades.module#PropiedadesPageModule' },
    { path: 'historial', loadChildren: './historial/historial.module#HistorialPageModule' },
    { path: 'blog', loadChildren: './blog/blog.module#BlogPageModule' },
    { path: 'entrada-blog', loadChildren: './entrada-blog/entrada-blog.module#EntradaBlogPageModule' },
    { path: 'tarjetas', loadChildren: './tarjetas/tarjetas.module#TarjetasPageModule' },
    { path: 'addtarjeta', loadChildren: './addtarjeta/addtarjeta.module#AddtarjetaPageModule' },
    { path: 'payload-program', loadChildren: './payload-program/payload-program.module#PayloadProgramPageModule' },
    { path: 'payload-checkout', loadChildren: './payload-checkout/payload-checkout.module#PayloadCheckoutPageModule' },
    { path: 'tipo-pago', loadChildren: './tipo-pago/tipo-pago.module#TipoPagoPageModule' },
    { path: 'mapa', loadChildren: './mapa/mapa.module#MapaPageModule' },
    { path: 'payload-done', loadChildren: './payload-done/payload-done.module#PayloadDonePageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map