import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
var TabsPage = /** @class */ (function () {
    function TabsPage(alertController) {
        this.alertController = alertController;
        this.cart = "0";
        this.updateBadge();
    }
    TabsPage.prototype.presentAlertConfirm = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Nota',
                            message: 'Se abrira tu aplicacion de correo',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah');
                                    }
                                }, {
                                    text: 'Ok',
                                    handler: function () {
                                        window.open("mailto:natalia@listapp.com.co", '_system');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TabsPage.prototype.updateBadge = function () {
        var badgecart = localStorage.getItem('badgecart');
        this.cart = badgecart;
    };
    TabsPage.prototype.ionViewWillEnter = function () {
        this.updateBadge();
    };
    TabsPage = tslib_1.__decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: './tabs.page.html',
            styleUrls: ['./tabs.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.page.js.map