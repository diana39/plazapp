import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepassPage } from './repass.page';

describe('RepassPage', () => {
  let component: RepassPage;
  let fixture: ComponentFixture<RepassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepassPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
