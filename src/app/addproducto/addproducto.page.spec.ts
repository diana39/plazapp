import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddproductoPage } from './addproducto.page';

describe('AddproductoPage', () => {
  let component: AddproductoPage;
  let fixture: ComponentFixture<AddproductoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddproductoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddproductoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
