import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { Observable } from 'rxjs/Observable';
import { ServerService } from "../server/server.service";


@Component({
  selector: 'receta',
  templateUrl: './receta.page.html',
  styleUrls: ['./receta.page.scss'],
})
export class RecetaPage implements OnInit {
  post: any;
  postid: any;

  constructor(public navCtrl: NavController,public param: ParametrosService, private server: ServerService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.postid =parseInt(localStorage.getItem("receta-select"));
    this.post = JSON.parse(localStorage.getItem('recetas'));
    this.post = this.post.filter(item => item.id === this.postid)[0];
  }

  gokit(id: any){
    if (id === undefined) {
        this.server.showAlert("Receta sin Kit","La receta actual no contiene ningún Kit, pronto estará disponible, gracias!");
      } else {
        localStorage.setItem("product-select",id);
        this.navCtrl.navigateForward("addproducto");
      }
    console.log(id);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
