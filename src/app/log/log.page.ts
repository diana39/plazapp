import { Component, OnInit } from '@angular/core';
import { NavController, Platform, AlertController  } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { HomePage } from '../home/home.page';
import { ServerService } from '../server/server.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.page.html',
  styleUrls: ['./log.page.scss'],
})
export class LogPage implements OnInit {
  email: string = "";
  password: string = "";
  alertct;

  constructor(
  	private screenOrientation: ScreenOrientation,
    public platform: Platform,
    public navCtrl: NavController, 
    public keyboard: Keyboard,
	  private auth: AuthService,
    public fb: FormBuilder,
    public server: ServerService,
    public alertCtrl: AlertController
    ) {
      if (this.platform.is('cordova') ) {
    	this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT)
      }
  	}

  ngOnInit() {}

  login(){
    let error: string = '';
    let hay_error : boolean = false;
    if(this.password.length < 4){ error = 'Debes ingresar una contraseña de al menos 4 caracteres'; hay_error = true;}
    if(!this.server.validateEmail(this.email)){ error = 'Email inválido'; hay_error = true; }
    if (hay_error) {
      this.server.showAlert('Campos incorrectos',error);
    } else {
      this.server.presentLoadingDefault("Autenticando Usuario...");
      let datos = { email: this.email, password: this.password };
      this.auth.doLogin(datos).then((resp)=>{
        console.log("Autenticado:",resp);
        this.server.dismissLoading();
        this.navCtrl.navigateRoot("tabs/home");
      }).catch((err)=>{
        this.server.dismissLoading();
        if(err.code === "auth/wrong-password"){
          this.showAlert('Error al Ingresar','Contraseña errada, verifica!');
        }
        else{
          this.showAlert('Error al Ingresar','Usuario no registrado, verifica!'); 
        }
        console.log("error:",err);
      });
    } 
  }

  fbLogin(){
    this.server.presentLoadingDefault("Autenticando desde Facebook");
    this.auth.doFacebookLogin().then((resp)=>{
      console.log("Autenticado:",resp);
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("tabs/home")
    }).catch((err)=>{
      this.server.dismissLoading();
      this.showAlert("Error al Ingresar","Error al ingresar con Facebook, intente nuevamente.");
      console.log("error:",err);
    });
  }

  googleLogin(){
    this.server.presentLoadingDefault("Autenticando desde Google");
    this.auth.doGoogleLogin().then((resp)=>{
      console.log("Autenticado:",resp);
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("tabs/home");
      }).catch((err)=>{
      this.server.dismissLoading();
      this.showAlert("Error al Ingresar","Error al ingresar con Google, intente nuevamente.");
      console.log("error:",err);
    });
  }

  goRepass(){
    this.navCtrl.navigateForward("repass");
  }

  goReg(){
    this.navCtrl.navigateRoot("registro");
  }

  goTerminos(){
    this.navCtrl.navigateForward('terminos');
  }

  goIntro(){
    this.navCtrl.navigateBack('intro');
  }

  goHome(){
    this.navCtrl.navigateBack('tabs/home');
  }

  async showAlert(titulo,mensaje) {
    const alertct = await this.alertCtrl.create({
      header: titulo,
      message: mensaje,
      buttons: ['OK']
    });
    await alertct.present();
  } 

}
