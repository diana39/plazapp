import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyTelPage } from './verify-tel.page';

describe('VerifyTelPage', () => {
  let component: VerifyTelPage;
  let fixture: ComponentFixture<VerifyTelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyTelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyTelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
