import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PayloadCheckoutPage } from './payload-checkout.page';

import { ComponentsModule } from '../componentes/componentes.module'
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { PipesModule } from '../pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: PayloadCheckoutPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    PipesModule
  ],
  declarations: [PayloadCheckoutPage],
  providers : [
    FirebaseAnalytics
  ]
})
export class PayloadCheckoutPageModule {}
