import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-recetas-medicos',
  templateUrl: './recetas-medicos.page.html',
  styleUrls: ['./recetas-medicos.page.scss'],
})
export class RecetasMedicosPage implements OnInit {


  id : any;
  recetas : any;
  productosRecetas: any;
  constructor( 
    public loadingCtrl: LoadingController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.cargarData();
  }
  
  async cargarData(){
    this.id = await localStorage.getItem('id_medico');        
    this.recetas = JSON.parse(localStorage.getItem('recetas'));
    this.recetas = this.recetas.filter(item => parseInt(item.Medico) === parseInt(this.id));    
  }

  async Descripcion(item:any){
    await localStorage.setItem('receta',JSON.stringify(item));
    let loading = await this.loadingCtrl.create({});
    loading.present().then(() => {
        this.navCtrl.navigateRoot('receta-detalle');
      loading.dismiss();
    });
  }

  goBack() {
    this.navCtrl.navigateBack("tabs/shop-medicos");
  }

}
