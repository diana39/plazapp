import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecetasMedicosPageRoutingModule } from './recetas-medicos-routing.module';

import { RecetasMedicosPage } from './recetas-medicos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecetasMedicosPageRoutingModule
  ],
  declarations: [RecetasMedicosPage]
})
export class RecetasMedicosPageModule {}
