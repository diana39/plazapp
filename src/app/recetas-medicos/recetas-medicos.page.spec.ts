import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecetasMedicosPage } from './recetas-medicos.page';

describe('RecetasMedicosPage', () => {
  let component: RecetasMedicosPage;
  let fixture: ComponentFixture<RecetasMedicosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecetasMedicosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecetasMedicosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
