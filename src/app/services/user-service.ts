import { Injectable } from "@angular/core";
import 'rxjs/add/operator/toPromise';
import * as firebase from 'firebase/app';
import { FirebaseUserModel } from "./user-model";

@Injectable()
export class UserService {

  constructor(){}


  getCurrentUser(){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().onAuthStateChanged(function(user){
        let userModel = new FirebaseUserModel();
        if (user) {
          if(user.providerData[0].providerId == 'password'){
            //use a default image
            userModel.image = 'assets/imgs/avatar.jpg';
            userModel.name = user.displayName;
            userModel.provider = user.providerData[0].providerId;
            userModel.telefono =  localStorage.getItem('phoneUser');
            userModel.email=user.email;
            userModel.uid=user.uid;
            return resolve(userModel);
          }
          else{
            userModel.image = user.photoURL;
            userModel.name = user.displayName;
            userModel.provider = user.providerData[0].providerId;
            userModel.telefono =  localStorage.getItem('phoneUser');
            userModel.email=user.email;
            userModel.uid=user.uid;
            return resolve(userModel);
          }
        } else {
          reject('No user logged in');
        }
      })
    }).catch(error => {
      console.log("error, No user logged in");
    })
  }
}