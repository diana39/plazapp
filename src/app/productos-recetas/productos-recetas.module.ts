import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosRecetasPageRoutingModule } from './productos-recetas-routing.module';

import { ProductosRecetasPage } from './productos-recetas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosRecetasPageRoutingModule
  ],
  declarations: [ProductosRecetasPage]
})
export class ProductosRecetasPageModule {}
