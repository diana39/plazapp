import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-shop-medicos',
  templateUrl: './shop-medicos.page.html',
  styleUrls: ['./shop-medicos.page.scss'],
})
export class ShopMedicosPage implements OnInit {

  medicos : any;
  isloading = true;
  constructor(
      public loadingCtrl: LoadingController,
      private auth: AuthService,
      public navCtrl: NavController,
     ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
     this.medicos = JSON.parse(localStorage.getItem('medicos'));
  }
 

  async VerRecetas(id : any, nota: string){
    await localStorage.setItem('id_medico', id);
    await localStorage.setItem('nota_medico', nota);
    let loading = await this.loadingCtrl.create({});
    loading.present().then(() => {
        this.navCtrl.navigateRoot('recetas-medicos');
      loading.dismiss();
    });
  }

  goBack() {
    this.navCtrl.navigateBack("tabs/home");
  }

}
