import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';
import { UserService } from '../services/user-service';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';


@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {

  compras;
  isloading = true;
  sinpedidos = false;
  moneda = localStorage.moneda;
  unidadNegocio = "";

  constructor(private auth: AuthService,
    private user: UserService,
    public server: ServerService,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public nav: NavController,
    private firebaseAnalytics: FirebaseAnalytics) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {    
    this.unidadNegocio = localStorage.UnidadNegocio;
    this.HistorialPedidos();
    this.firebaseAnalytics.logEvent('page_home', { page: "home" })
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  ayuda() {
    this.nav.navigateForward("contacto");
  }

  async ver(pedId: any, item: any) {
    let loading = await this.loadingCtrl.create({});
    console.log('id ', pedId)
    console.log('item ', item)
    loading.present().then(() => {
      localStorage.setItem("idcompra", pedId);
      localStorage.setItem("CompraSelect", JSON.stringify(item))
      this.nav.navigateForward("ver-compra");
      loading.dismiss();
    });

  }

  getProductos() {
    this.user.getCurrentUser()
      .then(user => {
        this.auth.getGenKeyVal("Usuario", "uid", user.uid).then(data => {
          let indata: any;
          indata = data;
          let iduser = indata[0].id;
          this.auth.getGenKeyVal("Compra", "usuario", iduser).then(data => {
            this.compras = data;
            localStorage.setItem('Compras', JSON.stringify(data));
            console.log("compras: ", this.compras);
            this.isloading = false;
          })
        });
      });
  }

  HistorialPedidos() {
    let datos = {
      "uid": localStorage.uid,
      "UnidadNegocio": localStorage.UnidadNegocio
    };

    this.server.getHistorial(JSON.stringify(datos)).then((data) => {
      let resp: any = data;
      if (resp.ok) {
        this.compras = resp.pedidos
        let x = resp.pedido;
        localStorage.setItem('Compras', JSON.stringify(resp.pedidos));
        console.log("compras: ", this.compras);
        this.isloading = false;
        if (this.compras === undefined) {
          this.sinpedidos = true;
        }
      } else {
        console.log(resp);
        this.server.showAlert('Error Servidor', "No se pueden obtener los pedidos.");
      }
    }).catch((err) => {
      this.server.showAlert('Error Red', "No se pudo conectar con el servidor.");
    });
    // });
  }

  async verReferencia(item) {
    let mensaje = '';
    let loading = await this.loadingCtrl.create({});
    loading.present().then(async () => {
      this.server.getReferencia(item.id).then(data => {      
          mensaje = 'Referencia: ' + data['pedidos'][0]['Respuesta'];   
      });
      this.server.consultarOrden(item.id).then(data => {        
        if(data['errorCode'] === '43' || data['errorCode'] === '00') {
          mensaje += ', ' + data['clientMessage'];          
        } else {
          mensaje = '¡Error! por favor inténtelo nuevamente.';                    
        }       
        this.server.showAlert('Pedido No. PED' + item.id, mensaje);
        loading.dismiss();
      });
    });
  }
}
