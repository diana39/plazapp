import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { isThisQuarter } from 'date-fns';
import { AuthService } from '../services/auth.service';
import { ProductosRecetasPage } from '../productos-recetas/productos-recetas.page';

@Component({
  selector: 'app-receta-detalle',
  templateUrl: './receta-detalle.page.html',
  styleUrls: ['./receta-detalle.page.scss'],
})
export class RecetaDetallePage implements OnInit {

  constructor( public navCtrl: NavController,
               private auth: AuthService,
               private modalCtrl: ModalController) { }
 
 
  receta: any;
  productosRecetas: any;
  semanas: any[];
  idMedico: number;
  notaMedico: string = "";

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.notaMedico = localStorage.getItem('nota_medico')
    this.semanas = [];
    this.productosRecetas = [];
    this.cargarData();
  }

  async cargarData() {
    this.idMedico = parseInt(localStorage.getItem('id_medico'));
    this.receta = JSON.parse(localStorage.getItem('receta'));
    this.productosRecetas = JSON.parse(localStorage.getItem('productosRecetas'));

    this.productosRecetas = this.productosRecetas.filter(item => {
      if (!this.semanas.includes(item.Semana) && item['Receta'] == this.receta['id']) {
        this.semanas.push(item.Semana);
      }    
    });
  }

  goBack() { 
    this.navCtrl.navigateBack("recetas-medicos");
  }

  async addCart(semana: any) {
    const modal = await this.modalCtrl.create({
       component: ProductosRecetasPage,
       componentProps: {
         semana,
         recetaNombre: this.receta['Nombre']
       }
     });
     await modal.present();
  }

}