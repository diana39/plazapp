import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PayloadDonePage } from './payload-done.page';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { PipesModule } from '../pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: PayloadDonePage
  }
];

@NgModule({
  imports: [
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PayloadDonePage],
  providers : [
    FirebaseAnalytics
  ]
})
export class PayloadDonePageModule {}
