import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { UserService } from '../services/user-service';
import { ServerService } from '../server/server.service';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
selector: 'app-payload-done',
templateUrl: './payload-done.page.html',
styleUrls: ['./payload-done.page.scss'],
})
export class PayloadDonePage implements OnInit {
username;
dir;
range;
uid;
nombredia = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];
Link = "";
mensaje;
method;
usuario = JSON.parse(localStorage.user);
idPedido;
errorOrden: string;
compras;
v;  
texto;
total = localStorage.TotalPedido;
moneda = localStorage.moneda;
reference: string;
precioPedido: string;
monedaPais: string = localStorage.getItem('moneda');
telefonoCaracas: string;
RIFCaracas: string;


constructor(public navCtrl: NavController,
  public menuCtrl: MenuController,
  private user: UserService,
  public server : ServerService,
  private alertCtrl: AlertController,
  public loadingCtrl : LoadingController,
  private iab: InAppBrowser,
  private firebaseAnalytics: FirebaseAnalytics,
  private fb : Facebook) { }

ngOnInit() {
}

async ionViewWillEnter(){

  if(localStorage.getItem("link") !== null) {
    this.Link = "https://pagos.plaz.com.co/index.php/pagos/hogar/linkmp/"+String(localStorage.getItem("link"));
  } else {
    this.Link= "";
  }

  let nombre = this.usuario.nombre.split(" ");
  if(String(this.Link).length > 0) {
    this.mensaje = "Pago pendiente"; 
    this.texto = String(nombre[0])+" Hemos enviado un sms con tu link de pago. Una vez procesado, tu pedido será confirmado.";
    this.method = '1';
  }
  
  this.method = String(localStorage.getItem("method"));    

  if (this.method === '13') {
    this.cargarPedidos();
    this.getDatoSisApi();
    this.mensaje = "Pago pendiente";
  } else if(this.method === '1') {
    this.mensaje = "Pago pendiente";
  } else {
    this.mensaje = "Pedido Realizado";
    this.texto = String(nombre[0])+" hemos recibido tu pedido, gracias por tu compra";
  }  
     
  let carrito = localStorage.getItem('cart');
  let unidadNegocio = localStorage.getItem('UnidadNegocio');
  let datosUsuario = JSON.parse(localStorage.getItem('user'));

  if(carrito.includes('408') && unidadNegocio == "2") {      
    this.server.setUsuariosPDF(datosUsuario['email'], datosUsuario['uid']);
  }    
        
  /*clean*/
  let cart = [];
  let numcart = [];
  let unitcart = [];
  let badgecart = "0";
  let preciocart = [];
  let maduracioncart = [];  
  localStorage.removeItem("total");
  localStorage.setItem("cart",JSON.stringify(cart));
  localStorage.setItem("numcart",JSON.stringify(numcart));
  localStorage.setItem("unitcart",JSON.stringify(unitcart));
  localStorage.setItem("badgecart",badgecart);
  localStorage.setItem("preciocart",JSON.stringify(preciocart));
  localStorage.setItem("maduracioncart",JSON.stringify(maduracioncart));

  carrito = "";
  unidadNegocio = "";
  datosUsuario = "";

  let res = {
    "Currency" : localStorage.moneda,
    "Valor" : localStorage.subtotal
  }

  this.fb.logEvent("Purchase", res).then(res => {
    console.log("FACEBOOK RES -->", res);
  }).catch(err => {
    console.log("error Facebook "+err);
  });

  //this.eviaCorreo()

  this.username = this.usuario.nombre;

  //Get Dir
  this.dir = localStorage.Dir;
  //Get Range Time
  let rang = localStorage.getItem("range-time");
  let rangsplit = rang.split("|");
  let dia = new Date(rangsplit[0]);    
  this.range = this.nombredia[dia.getDay()]+" "+rangsplit[1]+" a "+rangsplit[2];
  this.range = rangsplit[0] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];

  await this.creaPedidoMySql();
}

eviaCorreo(){
  //this.user.getCurrentUser().then(user => {
    //this.analitics();
    this.uid = localStorage.uid;
    this.server.envioCorreoConfirmacion(this.uid, localStorage.referido).then((data) => {
      localStorage.removeItem("referido");
      this.method = String(localStorage.getItem("method"));
      console.log(this.method, "method =====");
      if(this.method === '1'){
        this.Link = data['link'];
        window.open(this.Link, '_system');
      }
    });
  //});
  try {
    this.analitics();
  } catch (error) {
    console.log(error)
  }
}

analitics(){
  this.firebaseAnalytics.logEvent('inapp-purchase', {page: "payload-done"})
  .then((res: any) => console.log(res))
  .catch((error: any) => console.error(error));
}

goBack() {
  this.navCtrl.navigateRoot("historial");
}

AbrirVentana(){
  window.open(this.Link, '_system');
  this.navCtrl.navigateRoot('historial');
}


async cargarPedidos() {
  let datos = {
    "uid": localStorage.uid,
    "UnidadNegocio": localStorage.UnidadNegocio
  };
  await this.server.getHistorial(JSON.stringify(datos)).then((data) => {
    let resp: any = data;
    if (resp.ok) {
      localStorage.setItem('Compras', JSON.stringify(resp.pedidos));
      this.idPedido = resp.pedidos[0]['id'];
      this.precioPedido = resp.pedidos[0]['total'];
      this.verReferencia(resp.pedidos[0]['id']);
    } else {
      console.log(resp);
      this.server.showAlert('Error Servidor', "No se pueden obtener los pedidos.");
    }
  }).catch((err) => {
    this.server.showAlert('Error Red', "No se pudo conectar con el servidor.");
  });
}

async verReferencia(id) {
  let loading = await this.loadingCtrl.create({});
  loading.present().then(async () => {
    setTimeout(() => {
      this.server.getReferencia(id).then(data => {                    
        if(data['errorCode'] === '99') {
          this.errorOrden = data['systemMessage'] ;            
        } else {
          this.reference = data['pedidos'][0]['Respuesta'];               
        }
      });  
      loading.dismiss();
    }, 1000);
  });
}

async consultarOrden() {
  let mensaje = '';
  let loading = await this.loadingCtrl.create({});
  loading.present().then(async () => {    
    this.server.consultarOrden(this.idPedido).then(data => {                  
      if(data['errorCode'] === '43' || data['errorCode'] === '00') {
        mensaje = data['clientMessage'];          
      } else {
        mensaje = '¡Error! por favor inténtelo nuevamente.';          
      }
      this.server.showAlert('Estado de la compra', mensaje);
      loading.dismiss();
    });
  });
}

async crearOrden() {
  let mensaje = '';
  let loading = await this.loadingCtrl.create({});
  loading.present().then(async () => {    
    this.server.crearOrden(this.idPedido).then(data => {                
      if(data['errorCode'] !== '00') {
        this.server.showAlert('¡Error!', 'Por favor inténtelo nuevamente');
      } else {
        mensaje += data['clientMessage'];
        this.errorOrden = '';
        this.cargarPedidos();
        this.server.showAlert('Estado de la compra', mensaje);
      }
      loading.dismiss();
    });
  });
}

async getDatoSisApi() {
  await this.server.getSisApiCaracas().then((data) => {
    let resp: any = data;
    console.log('resp ', resp);
    if (resp.ok) {
      this.telefonoCaracas = resp.data[0]["Usuario"];
      this.RIFCaracas = resp.data[0]["Clave"];
    } else {  
      this.server.showAlert('Error Servidor', "No se pueden obtener los datos bancarios.");
    }
  }).catch((err) => {
    this.server.showAlert('Error Red', "No se pudo conectar con el servidor.");
  });
}

FinalizaCompra(){
  console.log('link pago ', this.Link)
  if(String(this.Link).length > 0){
    window.open(this.Link, '_system');
    this.server.SmsLink(this.Link, localStorage.user).catch(error => {
      console.log(error);
    });
  }
}

async creaPedidoMySql() {
  if( localStorage.UnidadNegocio === '2' ) {
    let loading = await this.loadingCtrl.create({});
    loading.present().then(async () => {
      await this.server.addPedidoMysql(localStorage.getItem('uid')).then(data => {              
        if(data['ok']) {      
          loading.dismiss();
          this.FinalizaCompra();
        } else {     
          this.server.showAlert('Error: 002', '');  
          loading.dismiss();
        }
      });
    });   
  }
}

}
